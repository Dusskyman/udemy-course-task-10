import 'package:flutter/material.dart';

class TextInfo with ChangeNotifier {
  String _text = 'Start text';
  String get text {
    return _text;
  }

  void changeText(String text) {
    _text = text;
    notifyListeners();
  }
}
