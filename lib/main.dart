import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:udemy_task_10_provider/custom_widgets/just_text.dart';
import 'package:udemy_task_10_provider/providers/text_info.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => TextInfo(),
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var changeText = Provider.of<TextInfo>(context, listen: false);
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SimpleText(),
            SizedBox(
              height: 75,
              width: 150,
              child: RaisedButton(
                  color: Colors.blueAccent,
                  child: Text('Text of first button'),
                  onPressed: () {
                    changeText.changeText('Text of first button');
                  }),
            ),
            Padding(padding: EdgeInsets.all(10)),
            SizedBox(
              height: 75,
              width: 150,
              child: RaisedButton(
                  color: Colors.blueAccent,
                  child: Text('Text of second button'),
                  onPressed: () {
                    changeText.changeText('Text of second button');
                  }),
            ),
            Padding(padding: EdgeInsets.all(10)),
            SizedBox(
              height: 75,
              width: 150,
              child: RaisedButton(
                  color: Colors.blueAccent,
                  child: Text('Text of third button'),
                  onPressed: () {
                    changeText.changeText('Text of third button');
                  }),
            ),
            Padding(padding: EdgeInsets.all(10)),
            SizedBox(
              height: 75,
              width: 150,
              child: RaisedButton(
                  color: Colors.blueAccent,
                  child: Text('Text of fourth button'),
                  onPressed: () {
                    changeText.changeText('Text of fourth button');
                  }),
            )
          ],
        ),
      ),
    );
  }
}
