import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:udemy_task_10_provider/providers/text_info.dart';

class SimpleText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var text = (Provider.of<TextInfo>(context).text);
    return Container(
      width: 150,
      color: Colors.deepOrange,
      alignment: Alignment.center,
      child: Text(text),
    );
  }
}
